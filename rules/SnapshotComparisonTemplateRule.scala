package com.indeni.server.rules.library

import com.indeni.data.conditions.{TagsStoreCondition, True}
import com.indeni.ruleengine.expressions.cluster.SelectClusterComparisonExpression
import com.indeni.ruleengine.expressions.conditions.{Equals, Not}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.SelectTagsExpression
import com.indeni.server.metrics.DeviceStoreActor
import com.indeni.server.rules.{RuleContext, RuleMetadata, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

class SnapshotComparisonTemplateRule(context: RuleContext, ruleName: String, ruleFriendlyName: String,
                                          severity: AlertSeverity = AlertSeverity.ERROR,
                                          ruleDescription: String, metricName: String, isArray: Boolean, applicableMetricTag: String = null, alertDescription: String,
                                          alertItemsHeader: String = "Mismatching Cluster Members", descriptionMetricTag: String = null, descriptionStringFormat: String = "", baseRemediationText: String, metaCondition: TagsStoreCondition = True,
                                          includeSnapshotDiff: Boolean = true)(vendorToRemediationText: (String, String)*)
  extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata(ruleName, ruleFriendlyName, ruleDescription, severity)

  override def expressionTree: StatusTreeExpression = {

    val thisSnapshot = SelectClusterComparisonExpression.thisSnapshotExpression().mostRecent().noneable.withLazy
    val clusterSnapshot = SelectClusterComparisonExpression.clusterSnapshotExpression().mostRecent().noneable.withLazy
    val selectExpression = SelectClusterComparisonExpression(context.metaDao, DeviceStoreActor.TAG_DEVICE_ID,
      DeviceStoreActor.TAG_DEVICE_NAME, DeviceStoreActor.TAG_DEVICE_IP, "cluster-id", context.snapshotsDao, metricName)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), metaCondition),

      // What constitutes an issue
      if (applicableMetricTag != null)
        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.snapshotsDao, if (descriptionMetricTag == null) Set(applicableMetricTag) else Set(applicableMetricTag, descriptionMetricTag), withTagsCondition(metricName)),

          StatusTreeExpression(
            if (isArray) selectExpression.multi() else selectExpression.single(),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            Not(Equals(thisSnapshot, clusterSnapshot))

            // The Alert Item to add for this specific item
            ).withSecondaryInfo(
              scopableStringFormatExpression("${scope(\"" + applicableMetricTag + "\")}" + (if (descriptionMetricTag != null) " (${scope(\"" + descriptionMetricTag + "\")})" else "") + " on %s (%s)", SelectClusterComparisonExpression.clusterDeviceNameExpression(), SelectClusterComparisonExpression.clusterDeviceIpExpression()),
              if (includeSnapshotDiff) SnapshotDiffExpression(thisSnapshot, clusterSnapshot) else EMPTY_STRING,
              title = alertItemsHeader
          ).asCondition()
        ).withoutInfo().asCondition()
      else
        StatusTreeExpression(
          // The time-series we check the test condition against:
          if (isArray) selectExpression.multi() else selectExpression.single(),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          Not(Equals(thisSnapshot, clusterSnapshot))
          ).withSecondaryInfo(
            scopableStringFormatExpression("%s (%s)", SelectClusterComparisonExpression.clusterDeviceNameExpression(), SelectClusterComparisonExpression.clusterDeviceIpExpression()),
            if (includeSnapshotDiff) SnapshotDiffExpression(thisSnapshot, clusterSnapshot) else EMPTY_STRING,
            title = alertItemsHeader
        ).asCondition()

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression(alertDescription),
        ConditionalRemediationSteps(baseRemediationText, vendorToRemediationText:_*)
    )
  }
}

