package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.{And, Equals}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.scope.ScopeValueExpression
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class NextHopRouterInaccessibleRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("cross_vendor_next_hop_router_inaccessible", "All Devices: Next hop inaccessible",
    "indeni will review the routing table and identify when a next hop router is showing as FAILED or INCOMPLETE in the ARP table.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(
          // The time-series we check the test condition against:
          SelectSnapshotsExpression(context.snapshotsDao, Set("arp-table", "static-routing-table")).multi(),

          // The condition which, if true, we have an issue. Checked against the time-series we've collected
          StatusTreeExpression(
            IterateSnapshotDimensionExpression("arp-table", "static-routing-table"),
              And(
                Equals[String, String](
                  ScopeValueExpression("targetip").invisible("arp-table").optional(),
                  ScopeValueExpression("next-hop").invisible("static-routing-table").optional()
                ),
                Equals(
                  ScopeValueExpression("success").invisible("arp-table").optional(),
                  ConstantExpression(Some("0"))
                )
              )
          ).withSecondaryInfo(
              scopableStringFormatExpression("${scope(\"static-routing-table:next-hop\")}"),
              EMPTY_STRING,
              title = "Inaccessible Next Hops"
          ).asCondition()
        ).withoutInfo().asCondition()


      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        scopableStringFormatExpression("Some of the routes in this device have a next hop which is inaccessible."),
        ConditionalRemediationSteps("Determine why the next hops are not responding.",
          ConditionalRemediationSteps.VENDOR_CP -> "Trying pinging the next hop routers in the list above and resolve any connectivity issues one by one until all pings are successful.",
          ConditionalRemediationSteps.VENDOR_PANOS -> "Log into the device over SSH and review the output of \"show arp\" to identify failures."
        )
    )
  }
}
