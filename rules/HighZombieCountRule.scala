package com.indeni.server.rules.library

import com.indeni.data.conditions._
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.GreaterThanOrEqual
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.ruleengine.expressions.utility.NoneableExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan
import HighZombieCountRule.NAME

case class HighZombieCountRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "High_Threshold_of_Zombie_Count"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "High Threshold of Zombie Process Count",
    "The threshold for the number of zombie processes, which once it is crossed an alert will be issued.",
    UIType.DOUBLE,
    20.0)

  override val metadata: RuleMetadata = RuleMetadata(NAME, "Linux-based Devices: High number of zombie processes",
    "indeni will alert when there are too many zombie processes.",
    TimeSpan.fromMinutes(10), AlertSeverity.ERROR, Set(highThresholdParameter))

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("tasks-zombies").last
    val threshold: NoneableExpression[Double] = getParameterDouble(context, highThresholdParameter)

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("tasks-zombies"), denseOnly = false),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            GreaterThanOrEqual(
              actualValue,
              threshold)

      ).withRootInfo(
            getHeadline(),
            scopableStringFormatExpression("The number of zombie processes is %.0f, above the threshold of %.0f.", actualValue, threshold),
            ConstantExpression("Review the list of processes on the device to determine the possible root cause. To identify what zombie processes are running, you may run the following command: ps -ef | grep defunct\nIf a parent process on your system keeps creating zombies, you may need to file a support ticket with the associated software vendor.")

      ).asCondition()
    ).withoutInfo()
  }
}

object HighZombieCountRule {

  /* --- Constants --- */

  private[library] val NAME = "high_zombie_tasks_count"
}
