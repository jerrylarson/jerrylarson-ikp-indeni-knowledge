package com.indeni.server.rules.library.paloalto

import com.indeni.data.conditions.True
import com.indeni.ruleengine.expressions.conditions.{And, Equals}
import com.indeni.ruleengine.expressions.core._
import com.indeni.ruleengine.expressions.data.{SelectTagsExpression, SelectTimeSeriesExpression, TimeSeriesExpression}
import com.indeni.server.rules.{RuleContext, _}
import com.indeni.server.rules.library.{PerDeviceRule, RuleHelper}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity

case class TsAgentDownRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  override val metadata: RuleMetadata = RuleMetadata("panw_ts_agent_down", "Palo Alto Networks Firewalls: Terminal services agent(s) down",
    "If the active member of a cluster has one or more terminal services agents down, indeni will alert.", AlertSeverity.ERROR)

  override def expressionTree: StatusTreeExpression = {
    val inUseValue = TimeSeriesExpression[Double]("ts_agent_state").last
    val activeMemberValue = TimeSeriesExpression[Double]("cluster-member-active").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      And(
          StatusTreeExpression(
            // The time-series we check the test condition against:
            SelectTimeSeriesExpression[Double](context.tsDao, Set("cluster-member-active"), denseOnly = false),

            // The condition which, if true, we have an issue. Checked against the time-series we've collected
            Equals(activeMemberValue, ConstantExpression[Option[Double]](Some(1.0)))
          ).withoutInfo().asCondition(),
          StatusTreeExpression(

            // The additional tags we care about (we'll be including this in alert data)
            SelectTagsExpression(context.tsDao, Set("name"), withTagsCondition("ts_agent_state")),

              StatusTreeExpression(
                // The time-series we check the test condition against:
                SelectTimeSeriesExpression[Double](context.tsDao, Set("ts_agent_state"), denseOnly = false),

                // The condition which, if true, we have an issue. Checked against the time-series we've collected
                Equals(
                  inUseValue,
                  ConstantExpression[Option[Double]](Some(0.0)))

                // The Alert Item to add for this specific item
              ).withSecondaryInfo(
                  scopableStringFormatExpression("${scope(\"name\")}"),
                  scopableStringFormatExpression("The Terminal Services agent is not responsive.", inUseValue),
                  title = "Terminal Services Agents"
              ).asCondition()
          ).withoutInfo().asCondition())

      // Details of the alert itself
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("One or more Terminal Services agents are down."),
        ConstantExpression("Check why the Terminal Services agents listed are not communicating.")
    )
  }
}
