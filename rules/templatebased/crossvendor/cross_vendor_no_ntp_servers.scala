package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.ruleengine.expressions.data.SnapshotExpression
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class cross_vendor_no_ntp_servers(context: RuleContext) extends MultiSnapshotValueCheckTemplateRule(context,
  ruleName = "cross_vendor_no_ntp_servers",
  ruleFriendlyName = "All Devices: No NTP servers configured",
  ruleDescription = "Many odd and complicated outages occur due to lack of clock synchronization between devices. In addition, logs may have the wrong time stamps. indeni will alert when a device has no NTP servers configured.",
  metricName = "ntp-servers",
  alertDescription = "This system does not have an NTP server configured. Many odd and complicated outages occur due to lack of clock synchronization between devices. In addition, logs may have the wrong time stamps.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/itzik-assaraf/2/870/1b5\">Itzik Assaraf</a> (Leumi Card).",
  baseRemediationText = "Configure one or more NTP servers to be used by this device for clock synchronization.",
  complexCondition = RuleEquals(RuleHelper.createEmptyComplexArrayConstantExpression(), SnapshotExpression("ntp-servers").asMulti().mostRecent().noneable))(
  ConditionalRemediationSteps.VENDOR_F5 -> "Log into the Web interface and navigate to System -> Configuration -> Device -> NTP. Add the desired NTP servers and click \"update\"."
)
