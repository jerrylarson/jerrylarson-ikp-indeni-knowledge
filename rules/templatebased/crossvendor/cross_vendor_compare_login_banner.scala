package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}


/**
  *
  */
case class cross_vendor_compare_login_banner(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_compare_login_banner",
  ruleFriendlyName = "Clustered Devices: Login banner mismatch across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if the login banner setting is different.",
  metricName = "login-banner",
  isArray = false,
  alertDescription = "The members of a cluster of devices must have the same login banner settings.",
  baseRemediationText = "Review the settings of each device in the cluster and ensure they are the same.")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Use the "show banner mtod" command to display the configured MOTD banner.
      |2. Review the output of each device in the cluster and ensure that are same across the cluster.
      |3. For more information please review  the following CISCO Configuration guide:
      |https://www.cisco.com/c/m/en_us/techdoc/dc/reference/cli/n5k/commands/banner-motd.html""".stripMargin
)
