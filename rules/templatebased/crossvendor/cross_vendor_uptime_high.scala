package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{NumericThresholdOnDoubleMetricTemplateRule, ThresholdDirection, ConditionalRemediationSteps}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

/**
  *
  */
case class cross_vendor_uptime_high(context: RuleContext) extends NumericThresholdOnDoubleMetricTemplateRule(context,
  ruleName = "cross_vendor_uptime_high",
  ruleFriendlyName = "All Devices: Device uptime too high",
  ruleDescription = "Indeni will alert when a device's uptime is too high.",
  severity = AlertSeverity.ERROR,
  metricName = "uptime-seconds",
  threshold = TimeSpan.fromDays(365 * 10),
  thresholdDirection = ThresholdDirection.ABOVE,
  alertDescriptionFormat = "The current uptime is %.0f seconds. This alert identifies when a device has been up for a very long time and may need an upgrade.",
  baseRemediationText = "Upgrade the device. You may also change the alert's threshold, or disable the alert completely, if not needed.")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Use the "show version" NX-OS command to display the current system uptime.
      |2. Run the "show system reset-reason" to check the reason for the last reboot of the device.
      |3. Check if the installed NX-OS version is supported and review it for software bugs.""".stripMargin
)
