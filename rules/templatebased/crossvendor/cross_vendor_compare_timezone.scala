package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}

/**
  *
  */
case class cross_vendor_compare_timezone(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_compare_timezone",
  ruleFriendlyName = "Clustered Devices: Timezone mismatch across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if the timezone setting is different.",
  metricName = "timezone",
  isArray = false,
  alertDescription = "The members of a cluster of devices must have the same timezone settings.",
  baseRemediationText = """Review the settings of each device in the cluster and ensure they are the same.""")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Execute the show clock command to check the system time for both peer switches
      |2. Review the time-zone setting of each device in the cluster and ensure they are the same.
      |3. For more information about  NX-OS timezone configuration review the next link:
      |https://www.cisco.com/c/en/us/td/docs/switches/datacenter/sw/4_0/nx-os/fundamentals/configuration/guide2/fun_nx-os_book/fun_5sys_mgmt.html""".stripMargin
)
