package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}
import com.indeni.time.TimeSpan

/**
  *
  */
case class cross_vendor_mgmt_ha_sync_down(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_mgmt_ha_sync_down",
  howManyRepetitions = 2,
  ruleFriendlyName = "Management Devices: Management high-availability sync down",
  ruleDescription = "Alert if the HA sync between management devices is not operating correctly.",
  metricName = "mgmt-ha-sync-state",
  stateDescriptionComplexMetricName = "mgmt-ha-sync-state-description",
  applicableMetricTag = "name",
  alertItemsHeader = "Management Systems Affected",
  alertDescription = "The high availability (HA) sync isn't operating correctly.",
  baseRemediationText = "This may due to a variety of underlying issues.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Review http://supportcontent.checkpoint.com/solutions?id=sk54160"
)
