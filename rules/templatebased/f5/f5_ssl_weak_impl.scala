package com.indeni.server.rules.library.templatebased.f5

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

/**
  *
  */
case class f5_ssl_weak_impl(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "f5_ssl_weak_impl",
  ruleFriendlyName = "F5 Devices: SSL Ticketbleed vulnerability (CVE-2016-9244)",
  ruleDescription = "In February of 2017, F5 users were notified of a new vulnerability in certain versions of BIG-IP. indeni will alert if any devices are vulnerable.",
  metricName = "ssl-weak-impl",
  applicableMetricTag = "profile-name",
  descriptionMetricTag = "type",
  alertIfDown = false,
  alertItemsHeader = "Affected Profiles",
  alertDescription = "This device is vulnerable to CVE-2016-9244, also known as Ticketbleed. Review the affected profiles below.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  baseRemediationText = "Read https://support.f5.com/csp/article/K05121675")()
