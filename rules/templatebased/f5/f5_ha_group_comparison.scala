package com.indeni.server.rules.library.templatebased.f5

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class f5_ha_group_comparison(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "f5_ha_group_comparison",
  ruleFriendlyName = "F5 Devices: Non-identical HA-group configuration detected",
  ruleDescription = "indeni will identify when two F5 devices are part of a device group and alert if the HA-group configuration is different.",
  metricName = "f5-ha-group",
  isArray = true,
  alertDescription = "HA-groups are one of the ways to determine if an F5 cluster should fail over or not by keeping track of trunk health and/or specific pool statuses. Should a link in a trunk fail, or a pool member stop responding this could trigger a fail-over. To minimize the risk of flapping an active bonus is highly recommended. Since this configuration is not synchronized it is ideal for it to be identical in all units of the cluster. Even more so, since F5's recommended way of manually failing over a cluster with ha-groups is to change the weight of the ha-group members. This is easily forgotten once done, which in turn could lead to the system not failing over when components do fail.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  baseRemediationText = """Make sure that the HA-group configuration is exactly the same in both devices. You may optionally choose to ignore certain differences if they are intended.""")()
