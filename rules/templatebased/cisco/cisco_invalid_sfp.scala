package com.indeni.server.rules.library.templatebased.cisco

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

/**
  *
  */
case class cisco_invalid_sfp(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cisco_invalid_sfp",
  ruleFriendlyName = "Cisco Devices: Interface(s) in error-disable state",
  ruleDescription = "Interfaces can be put in err-disable state if certain errors are detected by the devices. indeni will alert if this happens.",
  metricName = "network-interface-invalid-sfp",
  applicableMetricTag = "name",
  alertIfDown = false,
  alertItemsHeader = "Affected Interfaces",
  alertDescription = "Interfaces can be put in err-disable state if certain errors are detected by the devices.\nThis includes:\n* Flapping links\n* Spanning Tree BPDUs detected with BPDU Guard enabled\n* Detected a physical loop\n* Uni-directional link detected by UDLD",
  baseRemediationText = """1. Use the "show interface" and "show interface status err-disabled" NX-OS commands to identify the reason for the err-disable interface state.
      |2. View information about the internal state transitions of the port by using the"show system internal ethpm event-history interface X/X" NX-OS command.
      |3. Review the logs for relevant findings by running the "show logging" NX-OS command.
      |4. After fixing the issue performs a "shut/no shut" on the port to re-enable it.
      |5. It is possible to enable automatic periodic err-disable recovery by using the "errdisable recovery cause" NX-OS configuration command. For more information please review  the following CISCO  NX-OS guide:
      |https://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus5000/sw/layer2/421_n1_1/b_Cisco_n5k_layer2_config_gd_rel_421_n1_1/Cisco_n5k_layer2_config_gd_rel_421_n1_1_chapter3.html#task_3B5CB60B4E8746FA900E16679C66B437""".stripMargin
)()
