package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.data.conditions.{Equals => DataEquals, Not => DataNot}
import com.indeni.ruleengine.expressions.conditions.{Equals => RuleEquals, Not => RuleNot, Or => RuleOr}
import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library._

/**
  *
  */
case class checkpoint_compare_securexl_setting_novsx(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "checkpoint_compare_securexl_setting_novsx",
  ruleFriendlyName = "Check Point Cluster (Non-VSX): SecureXL configuration mismatch across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the SecureXL settings are different.",
  metricName = "securexl-status",
  isArray = false,
  alertDescription = "The members of a cluster of Check Point firewalls must have the same SecureXL settings.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/gal-vitenberg/83/484/103\">Gal Vitenberg</a>.",
  baseRemediationText = """Compare the output of "fwaccel stat" across members of the cluster.""",
  metaCondition = !DataEquals("vsx", "true"))()
