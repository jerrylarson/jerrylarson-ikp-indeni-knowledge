package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.conditions.{And, GreaterThan, LesserThan}
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data._
import com.indeni.ruleengine.expressions.math.{MinusExpression, PlusExpression}
import com.indeni.ruleengine.expressions.utility.NowExpression
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import com.indeni.time.TimeSpan

case class GeoIPDatabaseTooOldRule(context: RuleContext) extends PerDeviceRule with RuleHelper {

  private[library] val highThresholdParameterName = "Ahead_Alerting_Threshold"
  private val highThresholdParameter = new ParameterDefinition(highThresholdParameterName,
    "",
    "Age Threshold",
    "How long since the last IP geolocation database's update should indeni alert.",
    UIType.TIMESPAN,
    TimeSpan.fromDays(90))

  override val metadata: RuleMetadata = RuleMetadata("f5_geo_ip_database_stale", "F5 Devices: IP geolocation database not up to date",
    "indeni will alert when the IP geolocation database hasn't been updated in a while.", AlertSeverity.ERROR, highThresholdParameter)

  override def expressionTree: StatusTreeExpression = {
    val actualValue = TimeSeriesExpression[Double]("geoip-database-version").last

    StatusTreeExpression(
      // Which objects to pull (normally, devices)
      SelectTagsExpression(context.metaDao, Set(DeviceKey), True),

      // What constitutes an issue
        StatusTreeExpression(

          // The additional tags we care about (we'll be including this in alert data)
          SelectTagsExpression(context.tsDao, Set("database"), True),

            StatusTreeExpression(
              // The time-series we check the test condition against:
              SelectTimeSeriesExpression[Double](context.tsDao, Set("geoip-database-version"), denseOnly = false),

              // The condition which, if true, we have an issue. Checked against the time-series we've collected
              LesserThan(
                actualValue,
                MinusExpression[Double](NowExpression(), getParameterTimeSpanForTimeSeries(context, highThresholdParameter)))

              // The Alert Item to add for this specific item
              ).withSecondaryInfo(
                scopableStringFormatExpression("${scope(\"database\")}"),
                scopableStringFormatExpression("Hasn't been updated since %s", doubleToDateExpression(actualValue)),
                title = "Affected Databases"
            ).asCondition()
        ).withoutInfo().asCondition()
    ).withRootInfo(
        getHeadline(),
        ConstantExpression("One or more IP geolocation databases haven't been updated in a while. This may result in service disruption if these databases are relied upon."),
        ConstantExpression("This data is not automatically updated and requires manual updates using packages from download.f5.com. Follow https://support.f5.com/csp/#/article/K11176")
    )
  }
}
