package com.indeni.server.rules.library

import com.indeni.data.conditions.True
import com.indeni.ruleengine._
import com.indeni.ruleengine.expressions.core.{StatusTreeExpression, _}
import com.indeni.ruleengine.expressions.data.SelectTagsExpression
import com.indeni.ruleengine.expressions.log.SelectLogsExpression
import com.indeni.ruleengine.expressions.log.SelectLogsExpression._
import com.indeni.ruleengine.expressions.scope.{ScopableExpression, ScopeValueExpression}
import com.indeni.ruleengine.expressions.{Expression, NoArgsExpression}
import com.indeni.server.logs.LogRequestParameters._
import com.indeni.server.params.ParameterDefinition
import com.indeni.server.params.ParameterDefinition.UIType
import com.indeni.server.rules._
import com.indeni.server.rules.config.expressions.DynamicParameterExpression
import com.indeni.server.rules.library.LogPerChassisBladeInterrogationRule.{NAME, _}
import com.indeni.server.sensor.models.managementprocess.alerts.dto.AlertSeverity
import org.joda.time.{DateTime, DateTimeZone, Duration}

import scala.language.reflectiveCalls

/**
  * Created by mor on 6/7/16.
  */
case class LogPerChassisBladeInterrogationRule(context: RuleContext) extends PerDeviceRule {

  val LogTimeIntervalInMinutes: Int = 2

  private val logSeverityParameter = new ParameterDefinition(LOG_SEVERITY_PARAMETER_NANE, "", "Minimum log severity",
    "Minimum log severity to include in results", UIType.INTEGER, LogSeverity.ERROR.id)

  private val logSearchQueryParameter = new ParameterDefinition(LOG_SEARCH_QUERY_PARAMETER_NAME, "",
    "Match string", "A Query string to match in log", UIType.TEXT, "")

  override val metadata: RuleMetadata = RuleMetadata(NAME, "Log per Chassis Blade Interrogation",
    "Informs on log.", AlertSeverity.ERROR, logSeverityParameter, logSearchQueryParameter)

  override def expressionTree: StatusTreeExpression = {

    val logSeverity = DynamicParameterExpression.withConstantDefault(context.parametersDao, metadata.name, logSeverityParameter.getName,
      ConfigurationSetId, logSeverityParameter.getDefaultValue.asInteger.toInt)

    val logSearchQuery = DynamicParameterExpression.withConstantDefault(context.parametersDao, metadata.name, logSearchQueryParameter.getName,
      ConfigurationSetId, logSearchQueryParameter.getDefaultValue.asString)

    val deviceHeadline = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String = "Device IP:" + scope.getVisible(IP_ADDRESS_KEY).get + " has matching log entries"

      override def args: Set[Expression[_]] = Set()
    }

    val logDescription = new ScopableExpression[String] {
      override protected def evalWithScope(time: Long, scope: Scope): String = scope.getInvisible(LogModelKey).get.toString

      override def args: Set[Expression[_]] = Set()
    }

    val currentTimeExpression = new NoArgsExpression[Long] {
      override def eval(time: Long): Long = DateTime.now(DateTimeZone.UTC).getMillis
    }.withLazy

    val startTimeExpression = new Expression[Long] {
      override def eval(time: Long): Long = currentTimeExpression.eval(time) - Duration.standardMinutes(LogTimeIntervalInMinutes).getMillis

      override def args: Set[Expression[_]] = Set(currentTimeExpression)
    }

    val asIssue = ScopeValueExpression(LogModelKey).invisible().exists()

    val ruleParameters = Map[String, Expression[_]](StartTimestamp -> startTimeExpression, StopTimestamp -> currentTimeExpression,
      Severity -> logSeverity, SearchQuery -> logSearchQuery)

    val logsQuery = SelectLogsExpression(context.logsDao, IP_ADDRESS_KEY, ruleParameters)
    val hasRelevantLogs = StatusTreeExpression(logsQuery, asIssue)
      .withSecondaryInfo(deviceHeadline, logDescription, title = "Logs").asCondition()

    val headline = ConstantExpression("Matching log entries were found")
    val description = ConstantExpression("Log entries matching rule criteria")

    val devicesQuery = SelectTagsExpression(context.metaDao, Set(DeviceKey, IP_ADDRESS_KEY), True)

    StatusTreeExpression(devicesQuery, hasRelevantLogs)
      .withRootInfo(headline, description, EMPTY_STRING)
  }
}

object LogPerChassisBladeInterrogationRule {

  /* --- Constants --- */

  private[library] val NAME = "log_per_chassis_blade_interrogation"

  private[library] val LOG_SEVERITY_PARAMETER_NANE = "Log_Severity"

  private[library] val LOG_SEARCH_QUERY_PARAMETER_NAME = "Log_Search_Query"

  private[library] val IP_ADDRESS_KEY = "ip-address"

  /* --- Inner Classes --- */

  private object LogSeverity extends Enumeration {
    type Severity = Value
    val CRITICAL, ERROR, WARNING, INFO, DEBUG = Value
  }
}

