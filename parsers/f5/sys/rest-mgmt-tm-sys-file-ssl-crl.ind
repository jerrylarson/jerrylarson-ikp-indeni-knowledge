#! META
name: f5-rest-mgmt-tm-sys-file-ssl-crl
description: Check when certificate revocation list files was last updated
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"

#! COMMENTS
crl-last-modified:
    why: |
        Certification revocation lists is used to let systems know that specific issued certificates are now invalid. It is crucial for these lists to be up to date in order to prevent unauthorized access.
    how: |
        This alert uses the iControl REST interface to verify that the existing revocation lists are up to date.
    without-indeni: |
        Login to the device over SSH enter TMSH. Issue this command: "list sys file ssl-crl". This will show the revocation lists and when they were last updated.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: Unknown

#! REMOTE::HTTP
url: /mgmt/tm/sys/file/ssl-crl?$select=name,sourcePath,lastUpdateTime
protocol: HTTPS

#! PARSER::JSON

_metrics:
    -
        _groups:
            $.items[0:]:
                _tags:
                    "im.name":
                        _constant: "crl-last-modified"
                    "name":
                        _value: name
                    "sourcePath":
                        _value: sourcePath
                _temp:
                    "lastUpdated":
                        _value: lastUpdateTime
        _transform:
            _value.double: |
                {
                    lastUpdated = "${temp.lastUpdated}"

                    dateUpdated = lastUpdated

                    #2017-02-01T03:05:58Z
                    sub(/T.*/,"",dateUpdated)

                    split(dateUpdated, dateArr, /\-/)
                    year = dateArr[1]
                    month = dateArr[2]
                    day = dateArr[3]

                    timeUpdated = lastUpdated

                    #2017-02-01T03:05:58Z
                    sub(/.*T/,"",timeUpdated)
                    #03:05:58Z
                    sub(/Z$/,"",timeUpdated)

                    split(timeUpdated, timeArr, ":")

                    hour = timeArr[1]
                    minute = timeArr[2]
                    second = timeArr[3]

                    print datetime(year, month, day, hour, minute, second)

                }
