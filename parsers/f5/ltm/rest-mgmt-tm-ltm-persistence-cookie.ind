 #! META
name: f5-rest-mgmt-tm-ltm-persistence-cookie
description: Track cookie persistence profiles without encryption
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"

#! COMMENTS
f5-cookied-persistence-encrypted:
    why: |
        Not encrypting persistence cookies discloses internal information such as internal IP, port and pool name. This information could be used by an attacker to gather information about your environment. 
    how: |
        indeni uses the iControl REST interface to extract the persistence profile configuration.
    without-indeni: |
        Login to the device's web interface and click on "Local Traffic" -> "Profile" -> "Persistence". This would show a list of the configured persistence profiles, their members and their availability. Look for profiles of the type "cookie" and verify that each of them has cookie entryption enabled. In case the configuration is divided in multiple partitions changing to the "All [Read-only]" partition is recommended. This information is also available by logging into the device through SSH, enter TMSH and executing the command "cd /;list ltm persistence cookie recursive". 
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/ltm/persistence/cookie?$select=fullPath,cookieEncryption,cookieEncryptionPassphrase
protocol: HTTPS

#! PARSER::JSON

_metrics:
    - #Find cookie persistence profiles with configured encryption
        _groups:
            "$.items[?(@.cookieEncryption == 'required' && @.cookieEncryptionPassphrase)]":
                _tags:
                    "im.name":
                        _constant: "f5-cookied-persistence-encrypted"
                    "im.dstype.displaytype":
                        _constant: "boolean"
                    "name":
                        _value: "fullPath"
                _value.complex:
                    value:
                        _constant: "true"
    - #Find cookie persistence profiles without configured encryption
        _groups:
            "$.items[?(@.cookieEncryption != 'required' && !@.cookieEncryptionPassphrase && fullPath != '/Common/cookie')]":
                _tags:
                    "im.name":
                        _constant: "f5-cookied-persistence-encrypted"
                    "im.dstype.displaytype":
                        _constant: "boolean"
                    "name":
                        _value: "fullPath"
                _value.complex:
                    value:
                        _constant: "false"
