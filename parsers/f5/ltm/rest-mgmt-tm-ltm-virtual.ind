 #! META
name: f5-rest-mgmt-tm-ltm-virtual
description: Determine use of automap, and if any wildcard forwarding servers listening on all VLANs exists.
type: monitoring
monitoring_interval: 60 minute
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"

#! COMMENTS
f5-automap-used:
    why: |
        Using automap is a great way to troubleshoot assymetric routing, but is considered not ideal in a busy live environment because of a risk of port exhaustion. In case of high amount of traffic it is better to create a "SNAT Pool" with multiple IP addresses on the member networks.
    how: |
        This alert uses the iControl REST interface to extract the use of automap on virtual servers.
    without-indeni: |
        Login to the device's web interface and click on "Local Traffic" and then "Virtual servers". For each of the Virtual Servers, verify that automap is not used as "Source Address Translation".
    can-with-snmp: true
    can-with-syslog: false
f5-wildcard-forwarding-servers:
    why: |
        It is generally not recommended to have a virtual server listening on all VLANs with a destination of any. This can short circuit any VLANs behind the load balancer and is not ideal in terms of security.
    how: |
        This alert uses the iControl REST interface to extract any virtual forwarding servers listening to all destinations and on all VLANs.
    without-indeni: |
        Login to the device's web interface and click on "Local Traffic" and then "Virtual servers". For each of the Virtual Servers, verify if it is listening to any destination and on all VLANs.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/ltm/virtual?$select=fullPath,sourceAddressTranslation,ipForward,destination,source,vlans
protocol: HTTPS

#! PARSER::JSON

_metrics:
    
    - # Select virtual servers with automap enabled
        _groups:
            "$.items[0:]":
                _tags:
                    "im.name":
                        _constant: "f5-automap-used"
                    "im.dstype.displaytype":
                        _constant: "boolean"
                    "name":
                        _value: "fullPath"
                _temp:
                    "sourceAddressTranslation":
                        _value: sourceAddressTranslation.type
        _transform:
            _value.complex:
                value: |
                    {
                        if("${temp.sourceAddressTranslation}" == "automap") {
                            print "true"
                        } else {
                            print "false"
                        }
                    }
    - #Find virtual servers listening to any destination and on all vlans
        _groups:
            $.items[0:]:
                _tags:
                    "im.name":
                        _constant: "f5-wildcard-forwarding-servers"
                    "name":
                        _value: "fullPath"
                _temp:
                    "destination":
                        _value: "destination"
                    "source":
                        _value: source
                    "vlanCount":
                        _count: "vlans"
                    "ipForward":
                        _count: "ipForward"
        _transform:
            _value.complex:
                value: |
                    {
                        if(match("${temp.destination}", /.*0\.0\.0\.0:+0.*/) && match("${temp.source}", /.*0\.0\.0\.0\/0.*/) && "${temp.vlanCount}" == 0 && "${temp.ipForward}" == 1){
                            print "true"
                        } else {
                            print "false"
                        }
                    }