#! META
name: bluecoat-system-memory
description: fetch memory stats
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: bluecoat
    "os.name": sgos

#! REMOTE::HTTP
url: "/System/memory?stats_mode=0"
protocol: HTTPS

#! PARSER::AWK
BEGIN {
    memtags["name"] = "RAM"
}

# System Memory Statistics
/^System Memory/ { FS=" " } # In the System Memory section, we use " "

/^SM13 / {
 gsub(/^SM13\s+/, "", $0) # Remove tag and whitespace
 gsub(/,/, "", $0) # Remove commas
 writeDoubleMetricWithLiveConfig("memory-free-kbytes", memtags, "gauge", "60", sprintf("%s.0", $0 / 1024), "Memory Free (KB)", "number", "")
}

/^SM101 / {
 gsub(/^SM101\s+/, "", $0) # Remove tag and whitespace
 gsub(/,/, "", $0) # Remove commas
 used = $0
 writeDoubleMetricWithLiveConfig("memory-used-kbytes", memtags, "gauge", "60", sprintf("%s.0", $0 / 1024), "Memory Used (KB)", "number", "")
}

/^SM10 / {
 gsub(/^SM10\s+/, "", $0) # Remove tag and whitespace
 gsub(/,/, "", $0) # Remove commas
 total = $0
 writeDoubleMetricWithLiveConfig("memory-total-kbytes", memtags, "gauge", "60", sprintf("%s.0", $0 / 1024), "Memory Total (KB)", "number", "")
}

END {
	 writeDoubleMetricWithLiveConfig("memory-usage", memtags, "gauge", "60", used / total * 100, "Memory Usage", "percentage", "")
}

