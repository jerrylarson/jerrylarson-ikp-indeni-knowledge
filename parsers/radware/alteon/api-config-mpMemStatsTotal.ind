#! META
name: api-config-mpMemStatsTotal
description: get the total size of the RAM 
type: monitoring
monitoring_interval: 59 minute 
requires:
    os.name: "alteon-os"
    vendor: radware
    or:
        -
            form.factor: vx
        -
            form.factor: standalone

#! COMMENTS
memory-free-kbytes:
    skip-documentation: true
memory-total-kbytes:
    skip-documentation: true
memory-usage:
    why: |
        Memory utilization from the Management Process is key. If the management process begins to peg the available memory heavily, the admin should be made aware as this is indicative of a lack of resources available. Determining total allocated for the device is crucial for determining threshold levels.
    how: |
        This script runs the "/config/mpMemStatsTotal" and "/config/mpMemStatsFree" to check the % utilization which is necessary for Indeni to determine if the Radware device has high memory utilization.
    without-indeni: |
        An administrator would need to log in to the device and run a CLI command or run the API commands.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        Can be done through Management GUI (Vision or Alteon VX).

#! REMOTE::HTTP
url: /config/mpMemStatsTotal
protocol: HTTPS

#! PARSER::JSON
_dynamic_vars:
    _dynamic:
        mpMemStatsTotal:
            _value: mpMemStatsTotal
_metrics:
    -
        _temp:
            mpMemStatsTotal:
                _value: mpMemStatsTotal
        _tags:
            name:
                _constant: "RAM" 
            "im.name":
                _constant: "memory-total-kbytes"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Memory - Total"
            "im.dstype.displayType":
                _constant: "kilobytes"
            "im.identity-tags":
                _constant: "name"
        _transform:
            _value.double: |
                {
                	print ${temp.mpMemStatsTotal} / 1024
                }

#! REMOTE::HTTP
url: /config/mpMemStatsFree
protocol: HTTPS

#! PARSER::JSON
_metrics:
    -
        _temp:
            mpMemStatsFree:
                _value: mpMemStatsFree
        _tags:
            name:
                _constant: "RAM" 
            "im.name":
                _constant: "memory-free-kbytes"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Memory - Free"
            "im.dstype.displayType":
                _constant: "kilobytes"
            "im.identity-tags":
                _constant: "name"
        _transform:
            _value.double: |
                {
                	print ${temp.mpMemStatsFree} / 1024
                }
    -
        _temp:
            mpMemStatsFree:
                _value: mpMemStatsFree
        _tags:
            name:
                _constant: "RAM" 
            "im.name":
                _constant: "memory-usage"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Memory - Usage"
            "im.dstype.displayType":
                _constant: "percentage"
            "im.identity-tags":
                _constant: "name"
            "resource-metric":
                _constant: "true"
        _transform:
            _value.double: |
                {
                	print (1 - ${temp.mpMemStatsFree} / ${dynamic.mpMemStatsTotal}) * 100
                }

