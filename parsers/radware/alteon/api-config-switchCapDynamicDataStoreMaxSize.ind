#! META
name: api-config-switchCapDynamicDataStoreMaxSize
description: fetch the maximum size of the dynamic data store
type: monitoring
monitoring_interval: 59 minute 
requires:
    os.name: "alteon-os"
    vendor: radware
    or:
        -
            form.factor: vadc
        -
            form.factor: standalone

#! COMMENTS
dynamic-data-store-limit:
    why: |
        It is very common to deploy custom scripts via AppShape++ with the Alteon. These scripts leverage the "dynamic data store" of the device, which can be resource intensive. We want to track what the limit is and compare to current utilization.
    how: |
        This script runs the "/config/switchCapDynamicDataStoreMaxSize" through the Alteon API gateway.
    without-indeni: |
        An administrator would need to log in to the device and run the CLI command "/info/sys/capacity" or run the API command "/config/switchCapDynamicDataStoreMaxSize".
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Can be done through Management GUI (Vision or Alteon VX).

#! REMOTE::HTTP
url: /config/switchCapDynamicDataStoreMaxSize
protocol: HTTPS

#! PARSER::JSON
_metrics:
    -
        _value.double:
            _value: switchCapDynamicDataStoreMaxSize
        _tags:
            "im.name":
                _constant: "dynamic-data-store-limit"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Dynamic Date Store - Limit"
            "im.dstype.displayType":
                _constant: "number"