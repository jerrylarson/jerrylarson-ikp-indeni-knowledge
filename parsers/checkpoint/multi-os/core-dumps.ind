#! META
name: chkp-os-list_coredump
description: Lists coredump files
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    or:
        -
            os.name: gaia
        -
            os.name: secureplatform
        -
            os.name: ipso
            
#! COMMENTS
core-dumps:
    why: |
        A core dump is created when a process crashes. This usually means that there is an issue that should be investigated.
    how: |
        The list of core dumps is retrieved by logging into the shell over SSH and retrieving the details of files found in /var/log/dump/usermode/, /var/tmp/*core* and /var/crash.
    without-indeni: |
        An administrator could login and manually look for core dumps, but depending on version and device they could be in different places.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 ls --full-time -d /var/log/dump/usermode/* ; ${nice-path} -n 15 ls --full-time -d /var/tmp/*core* ; ${nice-path} -n 15 ls --full-time -d /var/crash/*

#! PARSER::AWK

# -rw-r--r-- 1 admin root 2898543 2016-07-12 08:52:01.000000000 +0200 sshd.14383.core.gz
/^[rwx-]{10}/ {
	# Exclude some directories
	if ($9 !~ "/var/crash/bounds|/var/crash/minfree") {

		ifile++

		# starting data
		createDate=$6
		createTime=$7

		# year,month,day
		split(createDate,dateArray,"-")
		createYear=dateArray[1]
		createMonth=dateArray[2]
		createDay=dateArray[3]

		# time
		gsub(/\.[0-9]+/,"",createTime)
		split(createTime, createTimeArr,":")	
		
		# Create complex object array	
		files[ifile, "path"]=$9	
		files[ifile, "created"]=datetime(createYear,createMonth,createDay,createTimeArr[1],createTimeArr[2],createTimeArr[3])
	}
}

END {
	# Write complex metric
	writeComplexMetricObjectArray("core-dumps", null, files)
}
