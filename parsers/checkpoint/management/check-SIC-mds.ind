#! META
name: chkp-check-SIC-mds
description: check the status of the SIC for all the gateways on mds
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: checkpoint
    mds: true
    role-management: true
    os.name: gaia

#! COMMENTS
trust-connection-state:
    skip-documentation: true

#! REMOTE::SSH
COLUMNS=150 && export COLUMNS && ${nice-path} -n 15 mdsstat |grep CMA | awk '{gsub(/\|/,"",$3); print $3}' | while read name; do mdsenv $name && (mdsstat $name && netstat -anpt 2>&1 && cat $FWDIR/database/netobj_objects.C) | awk '// { if(gsub(/\:18192.*$/,"",$0) > 0){ ipArray[$NF] = "" }} /^\| CMA \|/ { vsName=$3; vsIp=$5; gsub(/\|/,"",vsName);} /^\t: \(.+$/ { if ( host && (sic == 1 && ipCount >= 1)) { print "ip: " ip " vsName: " vsName " vsIp: " vsIp " hostname: " host; } ip = ""; sic = 0; ipCount = 0; host=$0; gsub(/\t\: \(/,"",host) } /:sic_name/ { sic = 1 } /:ipaddr/ { gsub(/\(|\)/,"",$2); for (id in ipArray) { if ( $2 == id) { ipCount++; ip=id } } } END { if ( host && (sic == 1 && ipCount >= 1)) { print "ip: " ip " vsName: " vsName " vsIp: " vsIp " hostname: " host; } }'; done | (while read input; do vsName=$(awk -v var="$input" 'BEGIN { split(var,splitArr," "); print splitArr[4]; }'); vsIp=$(awk -v var="$input" 'BEGIN { split(var,splitArr," "); print splitArr[6]; }'); cmd=$(awk -v var="$input" 'BEGIN { split(var,splitArr," "); print "cpstat -h " splitArr[2] " os"; }'); echo "Process $input started"; mdsenv $vsName; ($cmd && echo "Result: $input") & sleep 0.1 ; pid=$(ps aux|grep "$cmd" |grep -v "grep" |awk '{print $2}') ;  PID_LIST+=" $pid"; done ; sleep 15; for id in $PID_LIST ; do  kill $id; done ) ; sleep 1; echo ; COLUMNS=150 && export COLUMNS && ${nice-path} -n 15 mdsstat |grep CMA | awk '{gsub(/\|/,"",$3); print $3}' | while read name; do mdsenv $name && (mdsstat $name && cat $FWDIR/database/netobj_objects.C); done
 
#! PARSER::AWK

############
# Why: Test if the SIC works between the management and gateway.
# How: Try to read OS information via cpstat command. Once we know which one are not responding correctly we will lookup their names in netobj_objects.C
# Script explanation:
# List all CMAs
# For each CMA, use mdsenv to go into its context
# Within the context, output useful information
# * information about the CMA
# * a list of IP adresses that communicate on port 18192 (shown globally across all CMAs, does not respect context)
# * the content of netobj_objects.C
# Since bash cannot hold variables between subshells (| and while loops) we will pipe data between the different parts of the command instead of storing them i variables.
# while loop 1 : For each CMA, go into its context and then get data from netstat, netobj_objects.C and mdsstat.
# Since the netstat comment doesnt respect the context of the CMA, we will get connections to all gateways for all CMAs, however "cpstat -h IP os" will not work unless the correct mdsenv is set first.
# To filter out which IP belongs to which CMA, we will parse the netobj_objects.C, since this file is in the correct context. 
# After filtering we will end up with output into the 2nd while loop, that looks like this: "ip: 10.10.6.14 vsName: lab-CP-MGMT-R7730_Management_Server vsIp: 10.10.6.10 hostname: lab-CP-MGMT-R7730_Management_Server"
# This information is then fed into the 2nd while loop.
# while loop 2: For each line input it will dissect the different data. First it will go into the correct context with mdsenv, using the data from "vsName"
# It will then start "cpstat -h $IP os" using the info from the input data. It will put the running command into the background, since it needs to run all cpstat commands in paralell and also record the PID. This is because we only have 30 seconds before the .ind times out.
# Since we run everything in parellel, the output on the terminal will not be in the order we run the command. The output itself doesnt contain any clue to this as well. Thus we are chaining the running command with a "&& echo $cmd" so we know from which command the output came from. 
# Since we are storing the PID for each command, we can make sure that all processes we have started will be terminated before the ind script is done.
# After running all commands we wait for output for 15 seconds, and then send a kill to all PIDs. The timeout is 35 seconds for cpstat command, so if we do not kill some might be left running and could send output at a later stage.
# Once this is done we also output the netobj_objects.C again for each CMA, since this time we need to look for all other devices that has SIC established, but might not have a connection in netstat (maybe the device is turned off).
###########

function addVsTags(tags) {
	tags["vs.ip"] = vsIp
    tags["vs.name"] = vsName
}

## Get status from cpstat command.

# Process ip: 10.10.6.52 vsName: MDM-VSX_Management_Server vsIp: 10.10.6.14 hostname: lab-CP-VSXVSLS-2-R7730 started
/^Process ip: / {
	# We have attempted to connect to this host. If we do not get a valid response we need to mark this one as fail.
	hostname = $9
	vsName = $5
	vsIp = $7
	ip = $3
	status = 0
	
	iIndex = hostname ":" vsName
	statusArr[iIndex] = ip " " vsIp " " vsName " " hostname " " status 
}


# Product Name:                  SVN Foundation
/^Product Name:/ {
	# We got a successfull reply, but we dont yet know from which device. That information will come underneith the command output.
	status = 1
}

# Result: ip: 10.10.6.15 vsName: lab-CP-MGMT-R7730_Management_Server vsIp: 10.10.6.10 hostname: lab-CP-MGMT-R7730-HA
/^Result:/ {
	hostname = $9
	vsName = $5
	vsIp = $7
	ip = $3
	
	iIndex = hostname ":" vsName
	statusArr[iIndex] = ip " " vsIp " " vsName " " hostname " " status
}

## Get information from $FWDIR/database/netobj_objects.C 

# | CMA |MDM-VSX_Management_Server | 10.10.6.14      | up 5839    | up 5958  | up 5616  | up 7025  |
/^\| CMA \|/ { 
	vsName=$3; 
	vsIp=$5; 
	gsub(/\|/,"",vsName)
}

# : (lab-CP-GW5-R7710
/^\t: \(.+$/ { 
	if ( hostname && sic == 1) { 
		iIndex = hostname ":" vsName
		if (iIndex in statusArr) {
			# Do nothing, we have already written a status about this device
		} else {
			# If no status has been written after the cpstat command is done, then this device does not have a successfull connection.
			status = 0
			statusArr[iIndex] = tmpIpAddresses " " vsIp " " vsName " " hostname " " status
		}
	}
	tmpIpAddresses = ""
	ip = ""; 
	sic = 0; 
	hostname=$0; 
	gsub(/\t\: \(/,"",hostname) } 
	
# :sic_name ("CN=lab-CP-GW2-R7730,O=lab-CP-MGMT-R7730_Management_Server..nt8cac")	
/:sic_name/ {
	# SIC conneciton found. This means that the last hostname found is relevant.
	sic = 1 
} 

# :ipaddr (10.10.6.22)
/^\t\t:ipaddr \(|:mgmt_ip/ {
	# Record main IP for the above host.
	# Only catches main or mgmt IP
	ip = $2
	gsub(/\(|\)/,"",ip)
	if (ip != "0.0.0.0") {
		tmpIpAddresses = ip
	}
}

END {
	# Writing out the metric
	for (id in statusArr) {
		split(statusArr[id],splitArr," ")
		t["name"] = splitArr[4]
		t["ip"] = splitArr[1]
		addVsTags(t)
		status = splitArr[5]
		writeDoubleMetric("trust-connection-state", t, "gauge", 300, status)
	}
}
