#! META
name: chkp-gaia-show_sysenv
description: Show list of hardware status
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    os.name: gaia
    asg:
        neq: true

#! COMMENTS
hardware-element-status:
    why: |
        It is not uncommon for hardware components to fail inside a device without the device itself failing. In such an event they need to be replaced quickly.
    how: |
        Use clish "show sysenv all" command to list hardware health.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing hardware health is only available from the command line interface and WebUI.

#! REMOTE::SSH
${nice-path} -n 15 clish -c 'show sysenv all'

#! PARSER::AWK

# Seems like sometimes the status column doesn't change when hardware fail. This the script need to look at the minimum and maximum values and determine if the current value is ok or not.

#Name        Value       unit          type        status    Maximum     Minimum
#VCC          3.31         Volt          Voltage        0         3.9          2.7
#Vcore0       1.00         Volt          Voltage       -1         1.5          0.6
/ 0 / || / -1 / {
	type = $(NF-3)
	value = $(NF-5)
	
	max = $(NF-1)
	min = $NF
	
	# "min" can sometimes be the word "None". Replacing that with a negative number to make calculations work.
	if (min == "None") {
		min = -1000
	}
	
	# Start with assumption that its up, and change if we found anything wrong
	status = 1
	
	# The PSU reports status 0 both if up or down. Need to look for the word "Down" in the value instead
	if (value == "Down") {
		status = 0
	}
	
	# For numerical values we will see if the value is within the max and min values
	if (value < min || value > max) {
		status = 0
	}
	
	
	# Define name
	name = $1

	# For some lines the name need to be defined in another way
	# Power Supply #1   Up           On/Off        Power          0         Up           Down
	if ( name == "Power" ) {
		name = $1 " " $2 " " $3
	}
	
	# CPU0 TEMP    30.50        Celsius       Temperature    0         86.2         0
	# CPU0 FAN     5625.00      RPM           Fan            0         14000        600
	if ( name ~ /CPU*/ ) {
		name = $1 " " $2
	}

	hwTags["name"] = name
	hwTags["type"] = type
	writeDoubleMetricWithLiveConfig("hardware-element-status", hwTags, "gauge", 300, status, "Hardware Status", "state", "name")
}
