#! META
name: debug-status
description: checks if any debugs are running
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    role-firewall: true

#! COMMENTS
debug-status:
    why: |
        When troubleshooting a system debug flags are often enabled. When enabled they use extra resources, and forgetting to turn them off after troubleshooting has finished can mean service interruptions or reduced throughput.
    how: |
        By checking for known debug flags using Check Point built-in "fw ctl debug" command, as well as checking if tcpdump is running, the most common debugs can be alerted on if running for too long.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing debug flags is only available from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 fw ctl debug && ${nice-path} -n 15 ps aux |grep "[t]cpdump"

#! PARSER::AWK

# Kernel debugging buffer size: 50KB
/buffer size:/ {
	idebug++
	bufferSize = $NF
	gsub(/KB/,"",bufferSize)

	if ( bufferSize == "50") {
		debugStatus = 0
	} else {
		debugStatus = 1
	}
	t["name"] = "firewall kernel debug - bufferSize"
	writeDoubleMetric("debug-status", t, "gauge", 300, debugStatus)
}

# Module: fw
/Module:/ {
	idebug++
 	name = $NF
}

# Enabled Kernel debugging options: error warning
# Enabled Kernel debugging options: None
/debugging options:/ {
	# extract the module options
	moduleOptions = $0
	split(moduleOptions,moduleOptionsArr,":")
	moduleOptions = trim(moduleOptionsArr[2])
	
	debugStatus = 0
	# Check if the options are different than the default ones
	if ( name == "kiss" && moduleOptions != "error warning") {
		debugStatus = 1
	} else if ( name == "kissflow" && moduleOptions != "error warning") {
		debugStatus = 1
	} else if ( name == "fw" && moduleOptions != "error warning") {
		debugStatus = 1
	}else if ( name == "h323" && moduleOptions != "error") {
		debugStatus = 1
	} else if ( name == "CPAS" && moduleOptions != "error warning") {
		debugStatus = 1
	} else if ( name == "VPN" && moduleOptions != "err") {
		debugStatus = 1
	} else if (name != "None") {
		debugStatus = 0
	}
	
	# Write data
	t["name"] = "firewall kernel debug - " name "module"
	writeDoubleMetric("debug-status", t, "gauge", 300, debugStatus)
}


/tcpdump/ {
	t["name"] = "tcpdump"
	writeDoubleMetric("debug-status", t, "gauge", 300, 1)
}
