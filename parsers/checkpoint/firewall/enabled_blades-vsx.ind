#! META
name: chkp-enabled_blades-vsx
description: show enabled features/blades
type: monitoring
monitoring_interval: 60 minute
requires:
    vendor: checkpoint
    role-firewall: true
    os.name: gaia
    vsx: true

#! COMMENTS
features-enabled:
    why: |
        All members of a cluster should have the same features/blades enabled. By collecting this info it is possible to compare this between cluster member and alert if they do not have the same features enabled.
    how: |
        By using the Check Point built-in command "enabled_blades", a list of currently enabled software blades is retrieved. This command would need to be run on all members of a cluster.
    without-indeni: |
        An administrator could login and manually run the command or get the information from SmartDashboard.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        The best way is to retrieve this information is with Check Point SmartDashboard.

#! REMOTE::SSH
${nice-path} -n 15 fw vsx stat -l | grep VSID | awk '{print $NF}' | while read id; do vsenv $id && ${nice-path} -n 15 fw vsx stat $id && echo -n "enabled blade: " && ${nice-path} -n 15 enabled_blades ; echo -n "enabled blade: " && ${nice-path} -n 15 enabled_blades.sh; done

#! PARSER::AWK

#########
# Script explanation: According to sk105573 the command can be either enabled_blades or enabled_blades.sh.
# We will run both command to make sure, but since both command could exist at the same time we only want to store the output of one of them
########

function addVsTags(tags) {
	tags["vs.id"] = vsid
    tags["vs.name"] = vsname
}

BEGIN {
	vsid=""
	vsname=""
}
/VSID:/ {
	vsid = trim($NF)
	ifeat = 0
	metricWritten = 0
}
/Name:/ {
	vsname = trim($NF)
}

# nice: enabled_blades: No such file or directory
# fw vpn ips
/^enabled blade: [a-z]/ {
	if (metricWritten != 1 && $3 != "nice:") {
		split($0,splitArr," ")
		# Remove the first two entries
		delete splitArr[1]
		delete splitArr[2]
		for (id in splitArr) {
			ifeat++
			if (splitArr[id] != "") {
				features[ifeat, "name"] = splitArr[id]
			}
		}
		addVsTags(vsxtags)
		writeComplexMetricObjectArray("features-enabled", vsxtags, features)
		metricWritten = 1
		delete features
	}
}