#! META
name: chkp-vsx-stat-l
description: Lists VS's and get general stats
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: "checkpoint"
    vsx: "true"
    role-firewall: "true"

#! COMMENTS
concurrent-connections:
    skip-documentation: true

concurrent-connections-limit:
    skip-documentation: true

policy-name:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 fw vsx stat -l

#! PARSER::AWK

function addVsTags(tags) {
	tags["vs.id"] = vsid
	tags["vs.name"] = vsname
	tags["vs.type"] = type
}

BEGIN {
	FS = ":"
	vsid = ""
	vsname = ""
	type = ""
	policy = ""
}

#VSID:            0
/VSID:/ {
	vsid = trim($NF)
}

#Name:            VSX-CXL1-Metal
/Name:/ {
	vsname = trim($NF)
}

#Type:            VSX Gateway
#Type:            Virtual Switch
#Type:            Virtual System
/Type/ {
	type = trim($NF)
}

#Security Policy: Standard
#Security Policy: <Not Applicable>
/Security Policy/ {
	policy = trim($NF)
}


#Connections number: 2337
/Connections number.*\d/ {
	connections = trim($NF)
	# Connection data only applicable for elements of the VSX that have a policy
	if (policy != "") {
		addVsTags(t)
		writeDoubleMetricWithLiveConfig("concurrent-connections", t, "gauge", "60", connections, "Connections - Current", "number", "vs.id|vs.name")
	}
}

#Connections limit:  14900
/Connections limit.*\d/ {
	connectionsLimit = trim($NF)
	# Connection data only applicable for elements of the VSX that have a policy and if the limit is not zero.
	if (policy != "" && connectionsLimit != "0") {
		addVsTags(tags)
		writeDoubleMetricWithLiveConfig("concurrent-connections-limit", tags, "gauge", "60", connectionsLimit, "Connections - Capacity", "number", "vs.id|vs.name")
	}
}
