#! META
name: nexus-show-spanning-tree
description: fetch the Spanning Tree state
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: cisco
    os.name: nxos
    spanning-tree: true

#! COMMENTS
spanning-tree-root:
    why: |
       Capture the VLAN Spanning Tree root MAC address. A change in the Spanning Tree root is an indication of a topology change across a layer 2 broadcast domain.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the Spanning Tree state using the "show spanning-tree" command. The output includes a complete report of the Spanning Tree state across all VLANs.
    without-indeni: |
       STP Root changes do not generate syslog event. To identify STP topology changes the operator has to login to the device and use the "show spanning-tree" command to see which device is the current STP root and manually compare the previous state.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::SSH
show spanning-tree | xml

#! PARSER::XML
_omit:
    _bottom: 1
_vars:
    root: //__readonly__ 
_metrics:
    -
        _groups:
            ${root}/TABLE_tree/ROW_tree:
                _value.complex:
                    value:
                        _text: "tree_designated_root"
                _tags:
                    "im.name":
                        _constant: "spanning-tree-root" 
                    "name":
                        _text: "tree_id"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "Spanning Tree Root"
