#! META
name: nexus-show-ip-ospf-neighbors
description: Nexus show ip ospf neighbors
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: cisco
    os.name: nxos
    ospf: true

#! COMMENTS
ospf-state:
    why: |
       Check if OSPF neighbors are up. If an adjacency disappears an alert will be triggered.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the OSFP neigborship state using the "show ip ospf neighbor" command. The output includes a complete report of the per-neighbor OSPF state.
    without-indeni: |
       It is possible to poll this data through SNMP. State transitions generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true

#! REMOTE::SSH
show ip ospf neighbors

#! PARSER::AWK

#  Neighbor ID     Pri State            Up Time  Address         Interface
#  10.28.0.34        1 FULL/DR          40w0d    10.28.1.21      Eth1/2
/^\s*[1-9][0-9]*/ {
    neighbor_id=$1
    #priority=$2
    state=$3
    #up_time=$4
    #address=$5
    #interface=$6
    
    if (state ~ /FULL/) {
        ospf_state = "1.0"
    } else {
        ospf_state = "0.0"
    }

    ospftags["name"] = neighbor_id
    writeDoubleMetric("ospf-state", ospftags, "gauge", 60, ospf_state)
}


