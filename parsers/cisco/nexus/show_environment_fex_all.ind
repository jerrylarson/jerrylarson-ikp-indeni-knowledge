#! META
name: nexus-show-environment-fex-all
description: fetch fex system environment information
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: cisco
    os.name: nxos
    fex: true

#! COMMENTS
hardware-element-status:
    skip-documentation: true

#! REMOTE::SSH
show environment fex all | xml

#! PARSER::XML
_omit:
    _bottom: 1
_vars:
    root: //__readonly__
_metrics:
    -
        _groups:
            ${root}/powersup/TABLE_psinfo/ROW_psinfo:
                _temp:
                    ps_status:
                        _text: "ps_status"
                    psfex:
                        _text: "psfex"
                    psnum:
                        _text: "psnum"
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "FEX Power Supply Status"
                    "im.dstype.displayType":
                        _constant: "state"            
                    "im.identity-tags":
                        _constant: "name"
        _transform:
            _value.double:  |
                {
                    status = trim(tolower("${temp.ps_status}"))
                    if (status == "ok") {
                        print "1.0"
                    } else {
                        print "0.0"
                    }
                }
            _tags:
                "name": |
                    {
                        fex = trim("${temp.psfex}")
                        print "FEX " fex " Power Supply ${temp.psnum}"
                    }
    -
        _groups:
            ${root}/powersup/TABLE_mod_pow_info/ROW_mod_pow_info:
                _temp:
                    mod_status:
                        _text: "modstatus"
                    mod_fex:
                        _text: "modfex"
                    mod_num:
                        _text: "modnum"
                    mod_model:
                        _text: "mod_model"
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "FEX Module Power Status"
                    "im.dstype.displayType":
                        _constant: "state"
                    "im.identity-tags":
                        _constant: "name"
        _transform:
            _value.double:  |
                {
                    status = trim(tolower("${temp.mod_status}"))
                    if (status == "powered-up") {
                        print "1.0"
                    } else {
                        print "0.0"
                    }
                }
            _tags:
                "name": |
                    {
                        mod_num = trim("${temp.mod_num}")
                        mod_fex = trim("${temp.mod_fex}")
                        mod_model = trim("${temp.mod_model}")
                        print "FEX " mod_fex " Module " mod_num " (" mod_model ") Power"
                    }
    -
        _groups:
            ${root}/fandetails/TABLE_faninfo/ROW_faninfo:
                _temp:
                    fan_fex:
                        _text: "fanfex"
                    fan_status:
                        _text: "fanstatus"
                    fan_name:
                        _text: "fanname"
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "FEX Fan Status"
                    "im.dstype.displayType":
                        _constant: "state"            
                    "im.identity-tags":
                        _constant: "name"
        _transform:
            _value.double:  |
                {
                    status = trim(tolower("${temp.fan_status}"))
                    if (status == "ok") {
                        print "1.0"
                    } else {
                        print "0.0"
                    }
                }
            _tags:
                "name": |
                    fan_name = trim("${temp.fan_name}")
                    {
                        print "FEX ${temp.fan_fex} Fan " fan_name
                    }
    -
        _groups:
            ${root}/TABLE_tempinfo/ROW_tempinfo:
                _temp:
                    temp_fex:
                        _text: "tempfex"
                    temp_mod:
                        _text: "tempmod"
                    sensor:
                        _text: "sensor"
                    cur_temp:
                        _text: "curtemp"
                    thresh:
                        _text: "minthres"
                _tags:
                    "im.name":
                        _constant: "hardware-element-status"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "FEX Temperature Status"
                    "im.dstype.displayType":
                        _constant: "state"
                    "im.identity-tags":
                        _constant: "name"
        _transform:
            _value.double:  |
                {
                    cur_temp = trim("${temp.cur_temp}")
                    thresh = trim("${temp.thresh}")
                    if (cur_temp < thresh) {
                        print "1.0"
                    } else {
                        print "0.0"
                    }
                }
            _tags:
                "name": |
                    fex = trim("${temp.temp_fex}")
                    mod = trim("${temp.temp_mod}")
                    sensor = trim("${temp.sensor}")
                    {
                        print "Temperature FEX " fex " Module " mod " Sensor " sensor
                    }
                    









