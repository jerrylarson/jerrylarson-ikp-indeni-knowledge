#! META
name: nexus-show-module
description: Check the hardware modules state
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: cisco
    os.name: nxos
    hsrp_engine: true

#! COMMENTS
blade-state:
    why: |
       Report the status of the systems modules/line cards.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the modules state by using the "show module" command.
    without-indeni: |
       The administrator has to poll the data and monitor the state the modules manually.
    can-with-snmp: true
    can-with-syslog: true

#! REMOTE::SSH
show module | xml

#! PARSER::XML
_omit:
    _bottom: 1
_vars:
    root: //__readonly__ 
_metrics:
    -
        _groups:
            ${root}/TABLE_modinfo/ROW_modinfo:
                _temp:
                    modinf:
                        _text: "modinf"
                    modtype:
                        _text: "modtype"
                    model:
                        _text: "model"
                    status:
                        _text: "status"
                _tags:
                    "im.name":
                        _constant: "blade-state"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "Modules State"
                    "im.dstype.displayType":
                        _constant: "state"            
        _transform:
            _value.double:  |
                {
                    status = "0.0"
                    if ("${temp.status}" ~ /active/) {
                        status = "1.0"
                    }
                    if ("${temp.status}" ~ /ok/) {
                        status = "1.0"
                    }
                    if ("${temp.status}" ~ /standby/) {
                        status = "1.0"
                    }

                    print status
                }
            _tags:
                "name": |
                    { 
                        modinf = trim("${temp.modinf}")
                        model = trim("${temp.model}")
                        modtype = trim("${temp.modtype}")
                        print "Slot-" modinf ":" model " " modtype 
                    }
