#! META
name: nexus-show-feature
description: Nexus show features
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
features-enabled:
    why: |
        Collect information about features enabled on the device. When two Cisco Nexus switches participate in the same vPC, it is imperative to ensure that the same features are enabled on both of them.
    how: |
        This script logs into the Cisco Nexus switch through SSH and retrieves the list of enabled features using the "show feature" command.
    without-indeni: |
        It is impossible to retreive enabled features information through SNMP and no logs are generated for this information.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show feature | inc enabled

#! PARSER::AWK

BEGIN {
    i=0
}

#Feature Name          Instance  State
#--------------------  --------  --------
#bfd_app               1         disabled
#bgp                   1         enabled (not-running)
#bulkstat              1         disabled
#fcoe-npv              1         disabled
#fex                   1         enabled
/^.* 1 .*enabled$/ {
    features_arr[i, "name"] = $1
    i++
}

END {
    writeComplexMetricObjectArray("features-enabled", null, features_arr)
}
