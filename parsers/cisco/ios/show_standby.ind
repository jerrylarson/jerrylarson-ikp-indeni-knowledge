#! META
name: ios-hsrp-monitoring
description: Detemine system role in HSRP configuration
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: cisco
    os.name: ios 

#! COMMENTS
cluster-member-active:
    why: |
       Check if the device is the currently active HSRP (Hot Standby Router Protocol) member. The Hot Standby Routing Protocol is a redundancy protocol for establishing a fault-tolerant default gateway. The active router is acting as the gateway for the HSRP group.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the HSRP state using the "show hsrp" command. The output includes a complete report of the HSRP state across all configured interfaces.
    without-indeni: |
       It is possible to poll this data through SNMP. State transitions generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true

cluster-state:
    why: |
       Check if a configured HSRP (Hot Standby Router Protocol) group has at least one active member. If no active members exist, traffic would not be able to be routed.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the HSRP state using the "show hsrp" command. The output includes a complete report of the HSRP state across all configured interfaces.
    without-indeni: |
       It is possible to poll this data through SNMP. Only state transitions generate a syslog event. There is no explicit event for the last member failing.
    can-with-snmp: true
    can-with-syslog: false

cluster-preemption-enabled:
    why: |
       Check if an HSRP (Hot Standby Router Protocol) group has preemption enabled. If preemption is enabled then a recovering device can trigger a switchover which may create a short interruption in traffic forwarding. It is recommended to disable preemption.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the HSRP state using the "show hsrp" command. The output includes a complete report of the HSRP state across all configured interfaces.
    without-indeni: |
       It is possible to poll this data through SNMP. Preemption events would generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true
	
#! REMOTE::SSH
show standby
# show interfaces

#! PARSER::AWK

function resetVars () {
    interface = ""
    group = ""
    state = ""
    vip = ""
    mac = ""
    preempt = 0
    master = 0
}

BEGIN {
    # Initialize array indexes
    location = 0
    new_rec=0
    first_time=1
    metric_tags["live-config"] = "true"
}

# SAMPLE ####################################################
# FastEthernet3/0.1 - Group 31
#   State is Active
#   Virtual IP address is 192.168.31.254
#   Active virtual MAC address is 0000.0c07.ac1f (MAC In Use)
#   Preemption disabled
#   Active router is local
#   Standby router is 192.168.31.2, priority 190 (expires in 9.296 sec)
#   Priority 210 (configured 210)
#   Group name is "hsrp-Fa3/0.1-31" (default)
# FastEthernet3/0.2 - Group 32
#   State is Standby
#   Virtual IP address is 192.168.32.254
#   Active virtual MAC address is 0000.0c07.ac20 (MAC Not In Use)
#   Preemption enabled
#   Active router is 192.168.32.1, priority 210 (expires in 9.680 sec)
#   Standby router is local
#   Priority 190 (configured 190)
#   Group name is "hsrp-Fa3/0.2-32" (default)
# FastEthernet3/0.3 - Group 33
#   State is Listen
#   Virtual IP address is 192.168.33.254
#   Active virtual MAC address is 0000.0c07.ac21 (MAC Not In Use)
#   Preemption enabled
#   Active router is 192.168.33.1, priority 210 (expires in 10.224 sec)
#   Standby router is 192.168.33.2, priority 190 (expires in 10.656 sec)
#   Priority 170 (configured 170)
#   Group name is "hsrp-Fa3/0.3-33" (default)


# Line for HA entry, search for interface type at start of line
# Ethernet # FastEthernet # Forty # Gigabit # Hundred # Serial # Ten

/^[Ee]ther|^[Ff]ast|^[Ff]or|^[Gg]iga|^[Hh]un|^[Ss]er|^[Tt]en/ {
    new_rec = 1
    location++
    
    if (first_time = 1) {first_time=0} else {resetVars()}

    interface = trim($1)    
    split($0, parts, "-")
    split(parts[2], grp, " ")
    group = trim(grp[3])
    
    ha_entries[location, "interface"] = interface
    ha_entries[location, "group"] = group
}

/State is/ {
    state = trim($3)
    if (state == "Active") {state = 1}
    if (state == "Standby") {state = 0}
    if (state == "Listen") {state = 0}
    ha_entries[location, "state"] = state
}

/Virtual IP/ {
    vip = trim($5)
    ha_entries[location, "vip"] = vip
}

/Active virtual/ {
    mac = trim($6)
    ha_entries[location, "mac"] = mac
}

/[Pp]reemption/ {
    if (trim($2) == "enabled") {preempt = 1} else {preempt = 0}
    ha_entries[location, "preempt"] = preempt
}

/[Aa]ctive [Rr]outer/ {
    if (trim($4) == "local") {ha_entries[location, "master"]=1} else {ha_entries[location, "master"]=0}
}

/Group name/ {
}

END {
    for (i=1; i <= location; i++) {
        # writeDebug(ha_entries[i,"interface"])
        
        metric_tags["live-config"] = "true"
        metric_tags["name"] = "hsrp:" ha_entries[i, "interface"] ":"  ha_entries[i, "group"] ":" ha_entries[i, "vip"]
        metric_tags["im.identity-tags"] = "name"

        # Member State
        metric_tags["display-name"] = "HSRP - This Member State"
        metric_tags["im.dstype.displaytype"] = "state"
        if (state = "local") {state = 1} else {state =0}
        writeDoubleMetric("cluster-member-active", metric_tags, "gauge", 1, ha_entries[i, "master"])

        # Cluster State
        # 1.0 if at least one member in the cluster is active and handling traffic OK, 0.0 otherwise. Must have a "name" tag.
        metric_tags["display-name"] = "HSRP - Cluster State"
        metric_tags["im.dstype.displaytype"] = "state"
        metric_tags["group-vip"] = ha_entries[i, "vip"]
        # Ask Yoni / Arie about this one - three possible states : Active, Standby, Listen
        writeDoubleMetric("cluster-state", metric_tags, "gauge", 1, ha_entries[i, "state"])

        # Preemption
        metric_tags["display-name"] = "HSRP - Preemption"
        metric_tags["im.dstype.displaytype"] = "boolean"
        writeDoubleMetric("cluster-preemption-enabled", metric_tags, "gauge", 1, ha_entries[i, "preempt"])    
    }
}
