#! META
name: panos-show-vpn-flow
description: fetch the status of the VPN tunnels
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall

#! COMMENTS
vpn-tunnel-state:
    why: |
        VPN tunnels are one of the most critical features of a firewall. Many VPN tunnels are temporary, and may go up and down regularly, while some must be up at all times. Those that must remain up are usually set with a monitor to track their status. Knowing if a tunnel that should be up is down is critical for a quick response to service disruption.
    how: |
        This script uses the Palo Alto Networks API to retrieve the current status of the VPN tunnels (the equivalent of running "show vpn flow" in CLI). The script differentiates between alerts that are not "always on" (don't have a monitor set) and those that should be.
    without-indeni: |
        The VPN tunnel state is visible through the web interface. Normally, an administrator would access it after a service outage is reported.
    can-with-snmp: true
    can-with-syslog: true
vpn-tunnel-decryption-errors:
    why: |
        VPN tunnels are one of the most critical features of a firewall. Tracking the health of the VPN tunnels, and specifically if there are any decryption errors, is a good indicator of whether a tunnel is working as planned.
    how: |
        This script uses the Palo Alto Networks API to retrieve the current status of the VPN tunnels (the equivalent of running "show vpn flow" in CLI). The script retrieves the decryption errors for each tunnel.
    without-indeni: |
        Decryption error information is only accessible through the CLI to an administrator.
    can-with-snmp: false
    can-with-syslog: false
vpn-tunnel-authentication-errors:
    why: |
        VPN tunnels are one of the most critical features of a firewall. Tracking the health of the VPN tunnels, and specifically if there are any authentication errors, is a good indicator of whether a tunnel is working as planned.
    how: |
        This script uses the Palo Alto Networks API to retrieve the current status of the VPN tunnels (the equivalent of running "show vpn flow" in CLI). The script retrieves the authentication errors for each tunnel.
    without-indeni: |
        Authentication error information is only accessible through the CLI to an administrator.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><vpn><flow><%2Fflow><%2Fvpn><%2Fshow>&key=${api-key}
protocol: HTTPS

#! PARSER::XML
_vars:
    root: /response/result
_metrics:
    -
        _groups:
            ${root}/IPSec/entry:
                _temp:
                    state: 
                        _text: "state"
                    peerip:
                        _text: "peerip"
                    monitor:
                        _count: "monitor/on[. = 'True']"
                _tags:
                    "im.name":
                        _constant: "vpn-tunnel-state"
                    "live-config":
                       _constant: "true"
                    "display-name":
                        _constant: "VPN Tunnels - State"
                    "im.dstype.displayType":
                        _constant: "state"
                    "im.identity-tags":
                        _constant: "peerip"
        _transform:
            _value.double: |
                {
                    if ("${temp.state}" == "active") {print "1.0"} else {print "0.0"}
                }
            _tags:
                name: |
                    {
                        print "${temp.peerip} (IPSec)"
                    }
                peerip: |
                    {
                        print "${temp.peerip}"
                    }
                always-on: |
                    {
                        if (tolower("${temp.monitor}") > 0) {
                            print "true"
                        } else {
                            print "false"
                        }
                    }
    -
        _groups:
            ${root}/IPSec/entry:
                _temp:
                    peerip:
                        _text: "peerip"
                _tags:
                    "im.name":
                        _constant: "vpn-tunnel-decryption-errors"
                    "im.dsType":
                        _constant: "counter"
                    "live-config":
                       _constant: "true"
                    "display-name":
                        _constant: "VPN Tunnels - Decryption Errors"
                    "im.dstype.displayType":
                        _constant: "number"
                    "im.identity-tags":
                        _constant: "peerip"
                _value.double:
                    _text: "dec-err"
        _transform:
            _tags:
                name: |
                    {
                        print "${temp.peerip} (IPSec)"
                    }
                peerip: |
                    {
                        print "${temp.peerip}"
                    }
    -
        _groups:
            ${root}/IPSec/entry:
                _temp:
                    peerip:
                        _text: "peerip"
                _tags:
                    "im.name":
                        _constant: "vpn-tunnel-authentication-errors"
                    "im.dsType":
                        _constant: "counter"
                    "live-config":
                       _constant: "true"
                    "display-name":
                        _constant: "VPN Tunnels - Authentication Errors"
                    "im.dstype.displayType":
                        _constant: "number"
                    "im.identity-tags":
                        _constant: "peerip"
                _value.double:
                    _text: "auth-err"
        _transform:
            _tags:
                name: |
                    {
                        print "${temp.peerip} (IPSec)"
                    }
                peerip: |
                    {
                        print "${temp.peerip}"
                    }

