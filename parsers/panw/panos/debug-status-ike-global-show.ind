#! META
name: panos-debug-status-ike-global-show
description: grab the debug status of ike-global
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall

#! COMMENTS
debug-status:
    skip-documentation: true

#! REMOTE::SSH
debug ike global show

#! PARSER::AWK
BEGIN {
	# For each flag this command supports, we list what a "normal" value is
	normal["sw.ikedaemon.debug.global"] = "normal"

	# not supported yet: "debug vardata-receiver show"
}

# Debug status lines look like this:
# sw.ikedaemon.debug.pcap: False
# note that the value can be all kinds of things depending on what the debug element is
/[a-zA-Z\.\'\-\s]+\:\s*(\S)+/ {
	writeDebug($0)
	gsub(/(\{|\}|')/, "", $0) # Sometimes the output can be in JSON format like "{'md.apps.s1.mp.cfg.debug-level': 'info'}"
	flag = $1
	sub(/:$/, "", flag)
	debugtags["name"] = flag

	state = 1
	if (normal[flag] == $2) {
		state = 0
	}
	writeDoubleMetric("debug-status", debugtags,"gauge",3600,state)
}

END {
}
