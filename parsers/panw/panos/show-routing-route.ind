#! META
name: panos-show-routing-route
description: fetch the current routing table
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos

#! COMMENTS
static-routing-table:
    why: |
       Capture the route entries that are statically set on the device.
    how: |
        This script uses the Palo Alto Networks API to retrieve the current routing table (the equivalent of running "show routing route" in CLI).
    without-indeni: |
        An administrator would be able to poll this data through SNMP but additional external logic would be required to correlate the static routes table across cluster members.
    can-with-snmp: false
    can-with-syslog: false
connected-networks-table:
    why: |
        Capture the route entries that are directly connected to the device.
    how: |
        This script uses the Palo Alto Networks API to retrieve the current routing table (the equivalent of running "show routing route" in CLI).
    without-indeni: |
        An administrator would be able to poll this data through SNMP but additional external logic would be required to correlate the connected routes table across cluster members.
    can-with-snmp: false
    can-with-syslog: false
routes-usage:
    skip-documentation: true

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><routing><route><%2Froute><%2Frouting><%2Fshow>&key=${api-key}
protocol: HTTPS

#! PARSER::XML
_vars:
    root: /response/result
_metrics:
    -
        _value.double:
            _count: "${root}/entry"
        _tags:
            "im.name":
                _constant: "routes-usage"
            "live-config":
               _constant: "true"
            "display-name":
                _constant: "Routes - Count"
            "im.dstype.displayType":
                _constant: "number"
    -
        _groups:
            ${root}/entry[contains(flags, "S") and not(nexthop = '0.0.0.0')]:
                _tags:
                    "im.name":
                        _constant: "static-routing-table"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Static Routes"
                _temp:
                    destination:
                        _text: destination
                _value.complex:
                    "next-hop":
                        _text: nexthop
        _transform:
            _value.complex:
                mask: |
                    {
                        split("${temp.destination}", destparts, /\//)
                        if (destparts[2]) {
                            print destparts[2]
                        } else {
                            print "32"
                        }
                    }
                network: |
                    {
                        split("${temp.destination}", destparts, /\//)
                        if (destparts[1]) {
                            print destparts[1]
                        } else {
                            print "0.0.0.0"
                        }
                    }
        _value: complex-array
    -
        _groups:
            ${root}/entry[contains(flags, "C")]:
                _tags:
                    "im.name":
                        _constant: "connected-networks-table"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "Connected Networks"
                _temp:
                    destination:
                        _text: destination
        _transform:
            _value.complex:
                mask: |
                    {
                        split("${temp.destination}", destparts, /\//)
                        if (destparts[2]) {
                            print destparts[2]
                        } else {
                            print "32"
                        }
                    }
                network: |
                    {
                        split("${temp.destination}", destparts, /\//)
                        if (destparts[1]) {
                            print destparts[1]
                        } else {
                            print "0.0.0.0"
                        }
                    }
        _value: complex-array


