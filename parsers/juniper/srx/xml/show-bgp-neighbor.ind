#! META
name: junos-show-bgp-neighbor
description: JUNOS get BGP neighbor information
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: juniper
    os.name: junos

#! COMMENTS
bgp-received-routes:
    skip-documentation: true
bgp-state:
    why: |
        Due to the dynamic nature of BGP, it should be closely monitored to verify that it is working correctly. Since routing is a vital part of any network, a failure or issues in dynamic routing can cause large disruptions.
    how: |
        This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show bgp neighbor" command. The output includes the status of active/established/inactive BGP neighbors.
    without-indeni: |
        An administrator could login and manually run the command. SNMP traps and syslogs are also available to be aware of a BGP issue.
    can-with-snmp: true
    can-with-syslog: true
    vendor-provided-management: |
        Listing BGP neighbors is only available from the command line.

#! REMOTE::SSH
show bgp neighbor | display xml

#! PARSER::XML
_vars:
    root: /rpc-reply//bgp-information[1]
_metrics:
    -
        _groups:
            ${root}/bgp-peer:
                _tags:
                    "im.name":
                        _constant: "bgp-state"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "BGP - Neighbors"
                    "im.dstype.displayType":
                        _constant: "state"            
                    "im.identity-tags":
                        _constant: "name"
                _temp:
                    state:
                        _text: peer-state
                    name:
                        _text: peer-address
                    local_address:
                        _text: local-address
        _transform:
            _tags:
                "name": |
                   {
                        peer_port = "${temp.name}"
                        split(peer_port, peer, "\\+")
                        print peer[1]
                    }
                "local-address": |
                    {
                        local_port = "${temp.local_address}"
                        split(local_port, local_address, "\\+")
                        print local_address[1]
                    }
            _value.double: |
                {
                    if ("${temp.state}" == "Established") {
                        print "1"
                    } else {
                        print "0"
                    }
                }
    -
        _groups:
            ${root}/bgp-peer:
                _tags:
                    "im.name":
                        _constant: "bgp-received-routes"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "BGP Received Routes"
                    "im.dstype.displaytype":
                        _constant: "number"
                    "im.identity-tags":
                        _constant: "name"
                    "bgp-rib":
                        _text: bgp-rib/name
                _value.double:
                    _text: bgp-rib/received-prefix-count
                _temp:
                    name:
                        _text: peer-address
        _transform:
            _tags:
                "name": |
                    {
                        peerandport = "${temp.name}"
                        split(peerandport, peer, "\\+")
                        print peer[1]
                    }


