#! META
name: unix-ps_auxw
description: run "ps aux"
type: monitoring
monitoring_interval: 5 minutes
requires:
    or:
        -
            linux-based: true
        -
            linux-busybox: true
        -
            freebsd-based: true

#! COMMENTS
process-cpu:
    why: |
        If a single process is using a lot of CPU, this could indicate a problem.
    how: |
        List the CPU usage for all processes running.
    without-indeni: |
        An administrator could login and manually list this, it would have to be done while the problem is happening.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface.

#! COMMENTS
process-memory:
    why: |
        If a single process is using a lot of memory, this could indicate a problem.
    how: |
        List the memory usage for all processes running.
    without-indeni: |
        An administrator could login and manually list this, it would have to be done while the problem is happening.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        This is only accessible from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 ps aux

#! PARSER::AWK

# admin        1  0.0  0.0   2044   632 ?        Ss   Mar03   0:44 init [3]
/[0-9]/ {
    cpu = $3
    memory = $4
    pid = $2
    processname = $11
    command = $11
    for (i = 12; i < NF; i++) {
        command = command " " $i
    }

    pstags["name"] = pid
    pstags["process-name"] = processname
    gsub(/,/,"", command)
    pstags["command"] = command

    writeDoubleMetric("process-cpu", pstags, "gauge", "60", cpu)
    writeDoubleMetric("process-memory", pstags, "gauge", "60", memory)
}
