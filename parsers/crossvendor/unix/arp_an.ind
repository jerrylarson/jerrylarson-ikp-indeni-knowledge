#! META
name: unix-arp_an
description: run "arp -an"
type: monitoring
monitoring_interval: 10 minutes
requires:
    or:
        -
            linux-based: true
        -
            linux-busybox: true
        -
            freebsd-based: true

#! COMMENTS
arp-total-entries:
    why: |
        Tracking the number of ARP entries currently stored in the ARP cache is important to see if the amount is near the limit. If the limit is reached then a garbage collector which reapes old entries will be activated. This means that entries will be removed from the cache which can cause lag, since the device will need to ask for the MAC address again when sending a packet.
    how: |
        Using the built in "arp" command, the entries in the ARP cache are listed and can be counted.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing current entries in the ARP cache is only available from the command line interface.

arp-table:
    why: |
        The ARP table needs to be collected in order to alert if some MAC addresses can no longer be resolved. This is especially critical if the MAC addresses belong to next hop routers the firewall should be forwarding traffic to.
    how: |
        Using the built in "arp" command, the entries in the arp cache is listed.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing current entries in the ARP cache is only available from the command line interface.

static-arp:
    why: |
        A MAC/IP combination may be saved as a static ARP entry. If however the MAC address changes the device cannot communicate with the IP any longer. This issue will be very hard to troubleshoot. 
    how: |
        By running the linux command "arp -an" the currently configured static ARP entries is retreived.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Check Point: Listing static ARP entries is only available from the command line interface and WebUI.

#! REMOTE::SSH
${nice-path} -n 15 arp -an

#! PARSER::AWK

###############
# The -n is to not resolve the IPs with DNS.
###############

# ? (10.3.3.62) at 00:50:56:80:79:BA [ether] on eth0
# ? (10.3.3.211) at <incomplete> on eth0
# ? (10.3.3.211) at (incomplete)
/at/ {
    totalArps++
    targetip = $2
    gsub(/[\(\)]/, "", targetip)
    mac = $4

    iarp++

    arps[iarp, "targetip"]=targetip
    if (mac !~ /.*incomplete.*/) {
        arps[iarp, "mac"] = mac
        arps[iarp, "success"] = "1"
    } else {
        arps[iarp, "success"] = "0"
    }

    if ($0 ~ / on /) {
        interface = $NF
        arps[iarp, "interface"]=interface
    }
}

# ? (10.10.6.254) at 00:12:12:12:12:12 [ether] PERM on eth2
# ? (172.16.20.231) at 0:c:29:c0:94:bf permanent
/(PERM on|permanent)/ {
    iarp++
    targetip = $2
    gsub(/[\(\)]/, "", targetip)
    mac = $4
	
    staticArp[iarp, "ip-address"] = targetip
    staticArp[iarp, "mac-address"] = mac
}

END {
    writeDoubleMetric("arp-total-entries", null, "gauge", "60", totalArps)
    writeComplexMetricObjectArray("arp-table", null, arps)
    writeComplexMetricObjectArray("static-arp", null, staticArp)
}
